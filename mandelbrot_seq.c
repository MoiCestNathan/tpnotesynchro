/*
 * Sequential Mandelbrot program 
 * 
 * This program computes and saves the Mandelbrot set.
 * It examines all points in the complex plane
 * that have both real and imaginary parts between -2 and 2. 
 * It relies on qdbmp.h library to manipulate bmp images.  
 * 
 * To compile: gcc qdbmp.o mandelbrot_seq.c -o mandelbrot_seq
 * To execute:  ./mandelbrot_seq maxiter 
 * where 
 *   maxiter denotes the maximum number of iterations at each point
 * 
 * Output: a graphical image that is saved in the current repertory.
 *  
 * Code originally obtained from Web site http://www-inf.telecom-sudparis.eu/COURS/CSC5001/new_site/Supports/
 * 
 * Reformatted and revised by T. RAULT on july 2020
 */

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include "mandelbrot.h"
#include "qdbmp.h"

#define N           2           /* size of problem space (x, y from -N to N) */
#define NPIXELS     1500         /* size of display window in pixels */


/* ---- Main program ---- */

int main(int argc, char *argv[]) {
  uint maxiter;
  double r_min = -N;
  double r_max = N;
  double i_min = -N;
  double i_max = N;
  uint width = NPIXELS;         /* dimensions of display window */
  uint height = NPIXELS;

  double scale_r, scale_i;
  uint col, row, k;
  complex z, c;
  double lengthsq, temp;

  /* Check command-line arguments */
  if ((argc < 2) || (argc > 3)) 
  {
    fprintf(stderr, "usage:  %s maxiter \n", argv[0]);
    return EXIT_FAILURE;
  }

  /* Process command-line arguments */
  maxiter = atoi(argv[1]);  
	
  /* Create a new white image of size NPIXELS x NPIXELS */
  BMP*	bmp = BMP_Create(NPIXELS,NPIXELS,32);

  /***** Calculate and draw points *****/

  /* Compute factors to scale computational region to window */
  scale_r = (double) (r_max - r_min) / (double) width;
  scale_i = (double) (i_max - i_min) / (double) height;
  
  /* Calculate points and save their corresponding color */
  for (row = 0; row < height; ++row) {

    for (col = 0; col < width; ++col) {
      /* First term of the sequence */
      z.r = z.i = 0;

      /* Scale display coordinates to actual region, i.e. each pixel is associated with a complex number c */
      c.r = r_min + ((double) col * scale_r);
      c.i = i_min + ((double) row * scale_i);

      /* Calculate z0, z1, .... until divergence or maximum iterations */
      k = 0;
      do  {
	temp = z.r*z.r - z.i*z.i + c.r;
	z.i = 2*z.r*z.i + c.i;
	z.r = temp;
	// Compute the modulus of z (for a complex number z=a+b.i, modulus=|z|=sqrt(a*a+b*b))
	lengthsq = z.r*z.r + z.i*z.i;
	++k;
      } while (lengthsq < (N*N) && k < maxiter);

      /* Update the pixel's color */
      BMP_SetPixelRGB( bmp, row, col, 100+k*10, 155+k*20, 100 );

    }

  }
  /* Save new bmp image */
  BMP_WriteFile( bmp, "Mandelbrot_fractale.bmp" );
  BMP_CHECK_ERROR( stdout, -2 );
  /* Free all memory allocated for the image */
  BMP_Free( bmp );
  
  return EXIT_SUCCESS;
}

