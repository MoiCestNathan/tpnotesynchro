/*
 * Sequential Mandelbrot program
 *
 * This program computes and saves the Mandelbrot set.
 * It examines all points in the complex plane
 * that have both real and imaginary parts between -2 and 2.
 * It relies on qdbmp.h library to manipulate bmp images.
 *
 * To compile: gcc qdbmp.o mandelbrot_seq.c -o mandelbrot_seq
 * To execute:  ./mandelbrot_seq maxiter
 * where
 *   maxiter denotes the maximum number of iterations at each point
 *
 * Output: a graphical image that is saved in the current repertory.
 *
 * Code originally obtained from Web site http://www-inf.telecom-sudparis.eu/COURS/CSC5001/new_site/Supports/
 *
 * Reformatted and revised by T. RAULT on july 2020
 */


#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <pthread.h>
#include "mandelbrot.h"
#include "qdbmp.h"

#define N 2
#define NPIXELS     1500

typedef struct {
    uint maxiter;
    double r_min, r_max, i_min, i_max, scale_r, scale_i;
    uint width, height, thread_id, num_threads;
    BMP *bmp;
} thread_data;

pthread_mutex_t lock;

void *function_threads(void *arg) {
    thread_data *tdata = (thread_data *) arg;
    uint col, row, k;
    complex z, c;
    double lengthsq, temp;

    for (row = tdata->thread_id; row < tdata->height; row += tdata->num_threads) {
        for (col = 0; col < tdata->width; col++) {
            /* First term of the sequence */
            z.r = z.i = 0;
            /* Scale display coordinates to actual region, i.e. each pixel is associated with a complex number c */
            c.r = tdata->r_min + ((double) col * tdata->scale_r);
            c.i = tdata->i_min + ((double) row * tdata->scale_i);

            /* Calculate z0, z1, .... until divergence or maximum iterations */
            k = 0;
            do  {
                temp = z.r*z.r - z.i*z.i + c.r;
                z.i = 2*z.r*z.i + c.i;
                z.r = temp;
                // Compute the modulus of z (for a complex number z=a+b.i, modulus=|z|=sqrt(a*a+b*b))
                lengthsq = z.r*z.r + z.i*z.i;
                ++k;
            } while (lengthsq < (N*N) && k < tdata->maxiter);

            /* Update the pixel's color */
            pthread_mutex_lock(&lock);
            BMP_SetPixelRGB( tdata->bmp, row, col, 100+k*10, 155+k*20, 100 );
            pthread_mutex_unlock(&lock);
        }
    }

    pthread_exit(NULL);
}

int main(int argc, char *argv[]) {
    uint maxiter;
    double r_min = -N;
    double r_max = N;
    double i_min = -N;
    double i_max = N;
    uint width = NPIXELS;
    uint height = NPIXELS;
    uint num_threads;

    double scale_r, scale_i;
    uint col, row;

    if ((argc < 3) || (argc > 4)) {
        fprintf(stderr, "usage:  %s maxiter num_threads\n", argv[0]);
        return EXIT_FAILURE;
    }

    maxiter = atoi(argv[1]);
    num_threads = atoi(argv[2]);

    /* Create a new white image of size NPIXELS x NPIXELS */
    BMP *bmp = BMP_Create(NPIXELS, NPIXELS, 32);

    /* Compute factors to scale computational region to window */
    scale_r = (double) (r_max - r_min) / (double) width;
    scale_i = (double) (i_max - i_min) / (double) height;

    /* Create threads and set their arguments */
    pthread_t threads[num_threads];
    thread_data tdata[num_threads];

    for (int i = 0; i < num_threads; i++) {
        tdata[i].maxiter = maxiter;
        tdata[i].r_min = r_min;
        tdata[i].r_max = r_max;
        tdata[i].i_min = i_min;
        tdata[i].i_max = i_max;
        tdata[i].scale_r = scale_r;
        tdata[i].scale_i = scale_i;
        tdata[i].width = width;
        tdata[i].height = height;
        tdata[i].bmp = bmp;
        tdata[i].thread_id = i;
        tdata[i].num_threads = num_threads;
    }

    /* Launch threads */
    for (int i = 0; i < num_threads; i++) {
        pthread_create(&threads[i], NULL, function_threads, &tdata[i]);
    }

    /* Wait for threads to complete */
    for (int i = 0; i < num_threads; i++) {
        pthread_join(threads[i], NULL);
    }

    /* Save new bmp image */
    BMP_WriteFile(bmp, "Mandelbrot_fractale.bmp");
    BMP_CHECK_ERROR(stdout, -2);

    /* Free all memory allocated for the image */
    BMP_Free(bmp);

    return EXIT_SUCCESS;
}
