CC=gcc
CFLAGS=-Wall -Wextra
LDFLAGS=-lpthread

all: mandelbrot_par

qdbmp.o: qdbmp.c qdbmp.h
	gcc -c qdbmp.c

mandelbrot_par: qdbmp.o mandelbrot_par.c
	gcc qdbmp.o mandelbrot_par.c -o mandelbrot_par -lpthread

.PHONY: clean
clean:
	rm -f qdbmp.o mandelbrot_par
	rm Mandelbrot_fractale.bmp

.PHONY: run
run: mandelbrot_par
	./mandelbrot_par 800 4
